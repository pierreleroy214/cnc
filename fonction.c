#include <wiringPi.h>
#include <stdio.h>

#define DIRX 1
#define DIRY 5
#define DIRZ 2

#define PULX 0
#define PULY 4
#define PULZ 3

int demo(){
	
	//Just a fonction to see if all axes moves	

	int a;

	for(;;)
	{
		digitalWrite(1,HIGH);
		digitalWrite(2,HIGH);
		digitalWrite(5,HIGH);


		for(a=0;a<600;a=a+1)
		{
		
		digitalWrite(0,HIGH);
		digitalWrite(3,HIGH);
		digitalWrite(4,HIGH);
		delay(1);
		digitalWrite(0,LOW);
		digitalWrite(3,LOW);
		digitalWrite(4,LOW);
		delay(1);
		}
		
		digitalWrite(1,LOW);
		digitalWrite(2,LOW);
		digitalWrite(5,LOW);
	
		for(a=0;a<600;a=a+1)
		{
		
		digitalWrite(0,HIGH);
		digitalWrite(3,HIGH);
		digitalWrite(4,HIGH);
		delay(1);
		digitalWrite(0,LOW);
		digitalWrite(3,LOW);
		digitalWrite(4,LOW);
		delay(1);
		}
		printf("loop");
	}

	return 0;
}

int moveX(float travelDistance)
{
	if (travelDistance > 0){
		digitalWrite(DIRX,HIGH);
	}
	else{
		digitalWrite(DIRX,LOW);
		travelDistance = -1*travelDistance;
	}
	
	// 360 deg of the screw = 8mm

	int mmToStep;
	mmToStep = travelDistance*200/8;

	int nbStep;
	nbStep = (int)mmToStep;

	int i;
	i=0;

	for(i=0;i<nbStep;i=i+1){
		digitalWrite(PULX,HIGH);
		delay(1);
		digitalWrite(PULX,LOW);
		delay(1);
	}

	return 0;
}

int moveY(float travelDistance)
{
	if (travelDistance > 0){
		digitalWrite(DIRY,HIGH);
	}
	else{
		digitalWrite(DIRY,LOW);
		travelDistance = -1*travelDistance;
	}
	
	// 360 deg of the screw = 8mm

	int mmToStep;
	mmToStep = travelDistance*200/8;

	int nbStep;
	nbStep = (int)mmToStep;

	int i;
	i=0;

	for(i=0;i<nbStep;i=i+1){
		digitalWrite(PULY,HIGH);
		delay(1);
		digitalWrite(PULY,LOW);
		delay(1);
	}

	return 0;
}

int moveZ(float travelDistance)
{
	if (travelDistance > 0){
		digitalWrite(DIRZ,HIGH);
	}
	else{
		digitalWrite(DIRZ,LOW);
		travelDistance = -1*travelDistance;
	}
	
	// 360 deg of the screw = 8mm
	// x3 car poulie 20-60 dents

	int mmToStep;
	mmToStep = travelDistance*200*3.0/8.0;

	int nbStep;
	nbStep = (int)mmToStep;

	int i;
	i=0;

	for(i=0;i<nbStep;i=i+1){
		digitalWrite(PULZ,HIGH);
		delay(1);
		digitalWrite(PULZ,LOW);
		delay(1);
	}

	return 0;
}


int test(){
	moveZ(-150.0);
	moveX(-10.0);
	moveY(10.0);
	moveX(10.0);
	moveY(-30.0);
	moveX(-10.0);
	moveY(10.0);
	moveX(40.0);
	moveY(10.0);
	moveX(-30.0);
	moveZ(150.0);


	return 0;
}

int test2(){

	float x,y,z;
	x=y=z=0.0;

	moveZ(-18.0);

	for (z=0;z<10;z++){
		moveZ(-0.1*z);
		moveY(20.0);
		moveY(-20.0);
		for (x=0;x<20;x++){
			moveX(1);
			moveY(20.0);
			moveY(-20.0);
		}
		moveX(-20.0);
	}
	moveZ(51.0);


	return 0;
}

int main(void)
{
	wiringPiSetup();
	pinMode(PULX,OUTPUT); // STEP X
	pinMode(DIRX,OUTPUT); // DIR X
	pinMode(DIRZ,OUTPUT); // DIR Z
	pinMode(PULZ,OUTPUT); // STEP Z
	pinMode(PULY,OUTPUT); // STEP Y
	pinMode(DIRY,OUTPUT); // DIR Y
	
	test2();	

	return 0;
}

