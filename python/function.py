import RPi.GPIO as GPIO
import time
import array as arr

GPIO.setmode(GPIO.BCM)

GPIO.setup(27, GPIO.OUT) # X DIRECTION
GPIO.setup(22, GPIO.OUT) # X STEP
GPIO.setup(06, GPIO.OUT) # Y DIRECTION
GPIO.setup(05, GPIO.OUT) # Y STEP
GPIO.setup(26, GPIO.OUT) # Z DIRECTION
GPIO.setup(19, GPIO.OUT) # Z STEP

position = arr.array('d',[1,1,1]) # position de depart
destination = arr.array('d',[3,2,1]) #postion de destination
courses = arr.array('d',[0,0,0]) # course a parcourir

def calculCourse (pos,des):
    courses = arr.array('d',[0,0,0])
    courses[0] = des[0]-pos[0]
    courses[1] = des[1]-pos[1]
    courses[2] = des[2]-pos[2]
    return courses

courses = calculCourse(position,destination)
print(courses)

def mouvement(courses):

    X=courses[0]
    Y=courses[1]
    Z=courses[2]

    pasX = pasY = pasZ = 1

    if X>0:
        GPIO.output(27,GPIO.HIGH)
    else:
        GPIO.output(27,GPIO.LOW)

    if Y>0:
        GPIO.output(05,GPIO.HIGH)
    else:
        GPIO.output(05,GPIO.LOW)

    if Z>0:
        GPIO.output(19,GPIO.HIGH)
    else:
        GPIO.output(19,GPIO.LOW)

    nbStepX = X/pasX*200
    nbStepY = Y/pasY*200
    nbStepZ = Z/pasZ*200

    for i in range(200):
        GPIO.output(22,GPIO.HIGH)
        GPIO.output(06,GPIO.HIGH)
        GPIO.output(26,GPIO.HIGH)
        time.sleep(0.001)
        GPIO.output(22,GPIO.LOW)
        GPIO.output(06,GPIO.LOW)
        GPIO.output(26,GPIO.LOW)
        time.sleep(0.001)


mouvement(courses)
