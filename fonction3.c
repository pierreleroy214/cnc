#include <stdio.h>


#define DIRX 1
#define DIRY 5
#define DIRZ 2

#define PULX 0
#define PULY 4
#define PULZ 3
#define VITESSEMAXI 1
#define DISTANCEPARPASX 0.5
#define DISTANCEPARPASY 0.5
#define DISTANCEPARPASZ 0.5

float positionActuelle[] = {0,0,0};
float positionObjectif[] = {3,2,1};
int PUL[] = {DIRX,DIRY,DIRZ};

int moveTo(float positionObjectif[])
{
	printf("obj: %lf,%lf,%lf\n", positionObjectif[0],positionObjectif[1],positionObjectif[2]);
	float distanceAParcourir[] = {0,0,0};
	int nombreDePasAEffectuer[] = {0,0,0};
	float distanceParPas[] = {DISTANCEPARPASX,DISTANCEPARPASY,DISTANCEPARPASZ};
	float intervalTempsEntrePas[] = {0,0,0};
	float tempsMiniParcours = 0;
	float tempsLePlusLong = 0;
	float temps = 0;
	float incrementTemps = 0.01;
	float tempsAvantProchainPas[] = {0,0,0};
	int i = 0;
	for (i = 0;i<3;i++)
	{
		distanceAParcourir[i] = positionObjectif[i] - positionActuelle[i];
		tempsMiniParcours = distanceAParcourir[i] / VITESSEMAXI;
		nombreDePasAEffectuer[i] = distanceAParcourir[i] / distanceParPas[i];
		
		if (tempsMiniParcours > tempsLePlusLong)
		{
			tempsLePlusLong = tempsMiniParcours;
		}
		if (nombreDePasAEffectuer[i] != 0)
		{	
			intervalTempsEntrePas[i] = tempsLePlusLong / nombreDePasAEffectuer[i];
		}else{
			intervalTempsEntrePas[i]= -2;
		}
		tempsAvantProchainPas[i] = intervalTempsEntrePas[i];
	}
	printf("distance a parcourir: %lf,%lf,%lf\n", distanceAParcourir[0],distanceAParcourir[1],distanceAParcourir[2]);
	printf("temps mini parcours :%lf\n",tempsMiniParcours);
	printf("nombre de pas a effectuer: %d,%d,%d\n",nombreDePasAEffectuer[0],nombreDePasAEffectuer[1],nombreDePasAEffectuer[2]);
	printf("temps avant prochain pas: %lf,%lf,%lf\n",tempsAvantProchainPas[0],tempsAvantProchainPas[1],tempsAvantProchainPas[2]);
	while(distanceAParcourir[0] != 0 || distanceAParcourir[1] !=0  || distanceAParcourir[2] != 0)
	{
		printf("temps: %lf\n",temps);
		printf("distance a parcourir: %lf,%lf,%lf\n", distanceAParcourir[0],distanceAParcourir[1],distanceAParcourir[2]);
		printf("temps avant prochain pas: %lf,%lf,%lf\n",tempsAvantProchainPas[0],tempsAvantProchainPas[1],tempsAvantProchainPas[2]);
		for(i=0;i<3;i++)
		{
			tempsAvantProchainPas[i] = tempsAvantProchainPas[i] - incrementTemps;
			if(tempsAvantProchainPas[i] <= 0 && tempsAvantProchainPas[i] > -1)
			{
				//faire un pas
				//digitalWrite(PUL[i],HIGH);
				printf("pul\n");
				//delay(1);
				//digitalWrite(PUL[i],LOW);
				//delay(1);
				//diminuer distance restante
				distanceAParcourir[i] = distanceAParcourir[i] - distanceParPas[i];
				//si distance a parcourir non nul, remettre temps Avant prochain pas a l'intervcal de temps
				if(distanceAParcourir[i]>0)
				{
					tempsAvantProchainPas[i] = intervalTempsEntrePas[i] - incrementTemps;
				}
			}
		}
		if(temps > 4){
			break;
		}
		temps = temps + incrementTemps;
	}
	return 0;
}




int main(void)
{
	
	moveTo(positionObjectif);	

	return 0;
}

